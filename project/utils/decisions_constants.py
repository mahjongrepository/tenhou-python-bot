DRAW = "draw"

DISCARD_OPTIONS = "discard_options"
DISCARD = "discard"
DISCARD_SAFE_TILE = "discard_safe_tile"

STRATEGY_ACTIVATE = "activate_strategy"
STRATEGY_DROP = "drop_strategy"

INIT_HAND = "init_hand"

MELD_CALL = "meld"
MELD_PREPARE = "meld_prepare"
MELD_HAND = "meld_hand"
MELD_DEBUG = "meld_debug"

RIICHI = "riichi"

DEFENCE_THREATENING_ENEMY = "defence_threatening_enemy"
